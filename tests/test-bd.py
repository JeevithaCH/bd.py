import browndog.bd as bd
import json
import machine
import os


def main():
    """

    :return:
    """
    # Add your Brown Dog credentials here. If needed, you can change the BD API instance hostname too.
    url = "https://<username>:<password>@bd-api-dev.ncsa.illinois.edu"
    input_format = "png"
    input_filename = "ncsa.jpg"
    output_format = "png"
    output_path = "ncsa.png"
    input_filename_local = "dbpedia.txt"
    directory_path = "./"
    file_url = "https://browndog.ncsa.illinois.edu/examples/ncsa.jpg"
    filename = "ncsa_download.jpg"
    query_filename = "ncsa.jpg"

    docker_machine = None
    is_docker_machine_started = False
    verbose = True
    download = False

    extractor_name = "ncsa.image.ocr"
    parameters = {'apikey': ''}

    print "Current working directory: " + os.getcwd() + "\n"

    print "Test 1 Started"
    bds, username, password = test___split_url(url)
    print "Test 1 Completed\n"

    print "Test 2 Started"
    key = test_get_key(url)
    print "Test 2 Completed\n"

    print "Test 3 Started"
    token = test_get_token(bds, key)
    print "Test 3 Completed\n"

    # print "Test 4 Started"
    # json_content = test_check_token_validity(bds, token)
    # print "Test 4 Completed\n"
    #
    # print "Test 5 Started"
    # output_array = test_outputs(bds, input_format, token)
    # print "Test 5 Completed\n"
    #
    # print "Test 6 Started"
    # output = test_convert(bds, input_filename, output_format, output_path, token, verbose=verbose, download=download)
    # print "Test 6 Completed\n"
    #
    # print "Test 7 Started"
    # output = test_download_file(file_url, filename, token)
    # print "Test 7 Completed\n"
    #
    # print "Test 8 Started"
    # output = test_extractors(bds, "image/png", token)
    # print "Test 8 Completed\n"
    #
    # print "Test 9 Started"
    # output = test_extract(bds, input_filename, token)
    # print "Test 9 Completed\n"
    #
    # print "Test 10 Started"
    # output = test_index(bds, directory_path, token)
    # print "Test 10 Completed\n"
    #
    # print "Test 11 Started"
    # output = test_find(bds, query_filename, token)
    # print "Test 11 Completed\n"

    # Precursor code for testing methods that use docker. Uncomment the following block for the tests that follow.
    # Start of block

    # docker_machine = machine.Machine()
    # # Start default docker machine if not already started
    # if not docker_machine.exists():
    #     if verbose:
    #         print 'Starting default docker machine'
    #     is_docker_machine_started = docker_machine.start()
    # else:
    #     is_docker_machine_started = True
    # print "Docker Started: " + str(is_docker_machine_started)

    # End of block

    # print "Test 12 Started"
    # output = test_convert_local(bds, input_filename, output_format, output_path, token, docker_machine,
    # verbose=verbose)
    # print "Test 12 Completed\n"

    # print "Test 13 Started"
    # output = test_extract_local(bds, input_filename_local, token, docker_machine, verbose=verbose)
    # print "Test 13 Completed\n"

    print "Test 14 Started"
    output = test_extract_with_extractor_name(bds, input_filename, token, extractor_name=extractor_name)
    print "Test 14 Completed\n"

    print "Test 15 Started"
    output = test_extract_with_parameters(bds, input_filename, token, extractor_name=extractor_name,
                                          parameters=parameters)
    print "Test 15 Completed\n"


def test___split_url(url):
    """
    Test __split_url() method
    :return:
    """
    (bds, username, password) = bd.__split_url(url)
    print bds, username, password

    assert bds == "https://bd-api-dev.ncsa.illinois.edu"
    return bds, username, password


def test_get_key(url):
    """
    Test get_key() method
    :return:
    """
    key, code, reason = bd.get_key(url)
    print key, code, reason

    assert key is not None
    return key


def test_get_token(bds, key):
    """
    Test get_token() method
    :return:
    """

    # key = "af90decf-c81c-4848-8631-1768750d4e4a"  # correct
    # key = "af90decf-c81c-4848-8631-1768750d4e41"  # incorrect
    token, code, reason = bd.get_token(bds, key)
    print token, code, reason

    assert token is not None
    return token


def test_check_token_validity(bds, token):
    """
    Test check_token_validity() method
    :return:
    """

    json_response, code, reason = bd.check_token_validity(bds, token)
    print json_response, code, reason

    assert json_response is not None
    return json_response


def test_outputs(bds, input_format, token):
    """
    Test outputs() method
    :return:
    """

    output_array, code, reason = bd.outputs(bds, input_format, token)
    print output_array, code, reason

    assert isinstance(output_array, list) is True
    return output_array


def test_convert(bds, input_filename, output, output_path, token, wait=60, verbose=False, download=False):
    """
    Test convert() method
    :return:
    """

    output = bd.convert(bds, input_filename, output, output_path, token, wait, verbose=verbose, download=download)
    print output

    assert output is not None
    return output


def test_convert_local(bds, input_filename, output, output_path, token, docker_machine, wait=60, verbose=False):
    """
    Test convert_local() method
    :return:
    """

    output, code, reason = bd.convert_local(bds, input_filename, output, output_path, token, docker_machine, wait=wait,
                                            verbose=verbose)
    print output, code, reason

    assert output is not None
    return output


def test_download_file(file_url, input_filename, token, wait=60):
    """
    Test download_file() method
    :return:
    """

    output, code, reason = bd.download_file(file_url, input_filename, token, wait=wait)
    print output, code, reason

    assert output is not None
    return output


def test_extractors(bds, input_format, token):
    """
    Test extractors() method
    :return:
    """

    output, code, reason = bd.extractors(bds, input_format, token)
    print output, code, reason

    assert output is not None
    return output


def test_extract(bds, input_filepath, token, wait=60):
    """
    Test extract() method
    :return:
    """

    output, code, reason = bd.extract(bds, input_filepath, token, wait)
    print output, code, reason

    assert output is not None
    return output


def test_extract_local(bds, input_filename, token, docker_machine, wait=60, verbose=False):
    """
    Test extract_local() method
    :return:
    """

    output, code, reason = bd.extract_local(bds, input_filename, token, docker_machine, wait=wait, verbose=verbose)
    print output, code, reason

    assert output is not None
    return output


def test_index(bds, directory_path, token, wait=60, verbose=False):
    """
    Test index() method
    :return:
    """

    output, code, reason = bd.index(bds, directory_path, token, wait, verbose)
    print output, code, reason

    assert output is not None
    return output


def test_find(bds, query_filename, token, wait=60):
    """
    Test find() method
    :return:
    """

    output, code, reason = bd.find(bds, query_filename, token, wait)
    print output, code, reason

    assert output is not None
    return output


def test_extract_with_extractor_name(bds, input_filepath, token, extractor_name, wait=60):
    """
    Test extract() method with extractor name
    :return:
    """

    output, code, reason = bd.extract(bds, input_filepath, token, extractor_name=extractor_name, wait=wait)
    print output, code, reason

    assert output is not None
    return output


def test_extract_with_parameters(bds, input_filepath, token, extractor_name, parameters, wait=60):
    """
    Test extract() method with extractor name and parameters.
    :return:
    """

    output, code, reason = bd.extract(bds, input_filepath, token, extractor_name=extractor_name,
                                      parameters=parameters, wait=wait)
    print output, code, reason

    assert output is not None
    return output


if __name__ == '__main__':
    main()

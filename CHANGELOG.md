# Change Log for BD.py
All notable changes to this repo will be documented here.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.5.0]

### Added
- Extract method now also accepts specific extractor name and parameter JSON object. [BD-2216](https://opensource.ncsa.illinois.edu/jira/browse/BD-2216)

### Fixed
- Extractors method by filtering list of available extractors based on input file MIME type. [BD-2242](https://opensource.ncsa.illinois.edu/jira/browse/BD-2242)

## [0.4.0]

### Added
- Added CHANGELOG.MD

### Changed 
- Updated version number